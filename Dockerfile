FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-universal:3.0.0-12

USER root

RUN conda install \
  "cfgrib>=0.9.7,<0.9.10" \
  "contextily>=1.0,<1.2" \
  pathos \
  peewee \
  "proj!=9.0.0" \
  pytables \
  "rasterio>=1.2.7,<1.3"

RUN http_proxy=http://proxy.ethz.ch:3128 https_proxy=http://proxy.ethz.ch:3128 pip3 install --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
  pybufrkit \
  "salib==1.3.12" \
  climada

RUN conda clean --all

RUN chown -R 1000:1000 /opt/conda/share/proj

USER 1000
