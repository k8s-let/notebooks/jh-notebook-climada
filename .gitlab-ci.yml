# This is a GitLab CI configuration to build the project as a docker image
# The file is generic enough to be dropped in a project containing a working Dockerfile
# Author: Florent CHAUVEAU <florent.chauveau@gmail.com>
# Mentioned here: https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/

# do not use "latest" here, if you want this to work in the future
image: docker:20.10.14-dind

stages:
  - build
  - push
  - test

# Use this if your GitLab runner does not use socket binding
services:
  - name: docker:20.10.14-dind
    command: ["--tls=false"]

variables:
  DOCKER_HOST: tcp://localhost:2375
  DOCKER_TLS_CERTDIR: ""

.docker-script-before: &docker-script-before
  # docker login asks for the password to be passed through stdin for security
  # we use $CI_JOB_TOKEN here which is a special token provided by GitLab
  - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY


Trivy scan:
  stage: test
  # image: docker:stable
  tags:
    - k8s-runner
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [""]
  variables:
    # No need to clone the repo, we exclusively work on artifacts.  See
    # https://docs.gitlab.com/ee/ci/runners/README.html#git-strategy
    GIT_STRATEGY: none
    TRIVY_USERNAME: "$CI_REGISTRY_USER"
    TRIVY_PASSWORD: "$CI_REGISTRY_PASSWORD"
    TRIVY_AUTH_URL: "$CI_REGISTRY"
    FULL_IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  script:
    - trivy --version
    # cache cleanup is needed when scanning images with the same tags, it does not remove the database
    - time trivy image --clear-cache
    # update vulnerabilities db
    - time trivy --cache-dir .trivycache/ image --download-db-only --no-progress
    # Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
    - time trivy --cache-dir .trivycache/ image --exit-code 0 --no-progress --format template --template "@/contrib/gitlab.tpl"
        --security-checks vuln --output "$CI_PROJECT_DIR/gl-container-scanning-report.json" "$FULL_IMAGE_NAME"
    # Prints full report
    - time trivy --cache-dir .trivycache/ image --exit-code 0 --no-progress --security-checks vuln "$FULL_IMAGE_NAME"
    # Fails on high and critical vulnerabilities
    # After JupyterLab extension 3dmol
    - echo -e "CVE-2022-29078\nCVE-2021-3918\nCVE-2022-37601\nCVE-2021-44906\nCVE-2022-0686" > .trivyignore
    - time trivy --cache-dir .trivycache/ image --exit-code 1 --severity CRITICAL --no-progress --security-checks vuln "$FULL_IMAGE_NAME"
  cache:
    paths:
      - .trivycache/
  # Enables https://docs.gitlab.com/ee/user/application_security/container_scanning/ (Container Scanning report is available on GitLab EE Ultimate or GitLab.com Gold)
  artifacts:
    when:                          always
    reports:
      container_scanning:          gl-container-scanning-report.json


Build tag:
  # Build for a tag: write the tag, so the app can read and display the version
  before_script:
    - *docker-script-before
  stage: build
  tags:
    - k8s-runner
  only:
    - tags
  script:
    - echo Building version $CI_COMMIT_TAG
    # fetches the latest image (not failing if image is not found)
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    # builds the project, passing proxy variables, and vcs vars for LABEL
    # notice the cache-from, which is going to use the image we just pulled locally
    # the built image is tagged locally with the commit SHA, and then pushed to
    # the GitLab registry
    - >
      docker build
      --pull
      --build-arg http_proxy=$http_proxy
      --build-arg https_proxy=$https_proxy
      --build-arg no_proxy=$no_proxy
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --build-arg VCS_TAG=$CI_COMMIT_TAG
      --cache-from $CI_REGISTRY_IMAGE:latest
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
      .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

Build docker image:
  before_script:
    - *docker-script-before
  stage: build
  tags:
    - k8s-runner
  except:
    - tags
  script:
    # fetches the latest image (not failing if image is not found)
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    # builds the project, passing proxy variables, and vcs vars for LABEL
    # notice the cache-from, which is going to use the image we just pulled locally
    # the built image is tagged locally with the commit SHA, and then pushed to
    # the GitLab registry
    - >
      docker build
      --pull
      --build-arg http_proxy=$http_proxy
      --build-arg https_proxy=$https_proxy
      --build-arg no_proxy=$no_proxy
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --build-arg VCS_TAG=$CI_COMMIT_SHORT_SHA
      --cache-from $CI_REGISTRY_IMAGE:latest
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
      .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

# Here, the goal is to tag the "master" branch as "latest"
Push latest image:
  before_script:
    - *docker-script-before

  needs:
    - Build docker image
  variables:
    # We are just playing with Docker here.
    # We do not need GitLab to clone the source code.
    GIT_STRATEGY: none
  stage: push
  tags:
    - k8s-runner
  only:
    # Only "main" should be tagged "latest"
    - main
  script:
    # Because we have no guarantee that this job will be picked up by the same runner
    # that built the image in the previous step, we pull it again locally
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    # Then we tag it "latest"
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
    # Annnd we push it.
    - docker push $CI_REGISTRY_IMAGE:latest

# Every other branch
Push branch image:
  before_script:
    - *docker-script-before
  needs:
    - Build docker image
  variables:
    # We are just playing with Docker here.
    # We do not need GitLab to clone the source code.
    GIT_STRATEGY: none
  stage: push
  tags:
    - k8s-runner
  except:
    - main
    - tags
  script:
    # Because we have no guarantee that this job will be picked up by the same runner
    # that built the image in the previous step, we pull it again locally
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    # Then we tag it with the branch name
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    # And we push it.
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME


# Finally, the goal here is to Docker tag any Git tag
# GitLab will start a new pipeline everytime a Git tag is created, which is pretty awesome
Push tag image:
  before_script:
    - *docker-script-before
  needs:
    - Build tag
  variables:
    # Again, we do not need the source code here. Just playing with Docker.
    GIT_STRATEGY: none
  stage: push
  tags:
    - k8s-runner
  only:
    # We want this job to be run on tags only.
    - tags
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

